import { Engine, Scene, FreeCamera, Vector3, HemisphericLight, SceneLoader, ActionManager, ExecuteCodeAction, AssetsManager } from "@babylonjs/core";
import { LoadingScreen } from "./LoadingScreen";
import ADTFileLoader from "adtloader";
import WDTFileLoader from "wdtloader";
 
export default class App {
    engine: Engine;
    scene: Scene;

    constructor(readonly canvas: HTMLCanvasElement) {
        this.engine = new Engine(canvas)
        window.addEventListener('resize', () => {
            this.engine.resize();
        });
	let ADTLoader = new ADTFileLoader();
	let WDTLoader = new WDTFileLoader();
	SceneLoader.RegisterPlugin(ADTLoader);
	SceneLoader.RegisterPlugin(WDTLoader);
	this.scene = createScene(ADTLoader, this.engine, this.engine);
    }

    run() {
        this.engine.runRenderLoop(() => {
            this.scene.render();
        });
    }
}

var createScene = function (adtLoader: ADTFileLoader, engine: Engine, canvas: HTMLCanvasElement) {

    var scene = new Scene(engine);

    var camera = new FreeCamera("camera1", new Vector3(0, 5, -10), scene);
    camera.setTarget(Vector3.Zero());

    camera.attachControl(canvas, true);

    var light = new HemisphericLight("light", new Vector3(0, 1, 0), scene);
    light.intensity = 0.7;
    let character: any; 
    let wdtContent
    SceneLoader.ImportMeshAsync("", "assets/", "Azeroth.wdt", scene).then((result) => {
	wdtContent = result;
});
    adtLoader.loadWDT(wdtContent);

    SceneLoader.ImportMeshAsync("", "assets/", "Azeroth_32_41.adt", scene);

    scene.actionManager = new ActionManager(scene);

    let inputMap: any = {};
    scene.actionManager.registerAction(new ExecuteCodeAction(ActionManager.OnKeyDownTrigger, (evt) => {
        inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
    }));
    scene.actionManager.registerAction(new ExecuteCodeAction(ActionManager.OnKeyUpTrigger, (evt) => {
        inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keyup";
    }));

    scene.onBeforeRenderObservable.add(() => {
        updateFromKeyboard(inputMap, character);
    });

    return scene;
};

const updateFromKeyboard = (inputMap: any, player: any) => {
    if (inputMap["z"]) {

    } else if (inputMap["ArrowDown"]) {

    } 

    if (inputMap["ArrowLeft"]) {


    } else if (inputMap["ArrowRight"]) {

    }
}
